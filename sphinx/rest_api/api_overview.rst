Overview
===========================
The Qubole Data Service (QDS) is accessible via REST APIs.

To write and test applications that use these APIs, users can use any HTTP client in any
programming language to interact with Qubole Data Service. The detailed syntax of the calls is
described in subsequent sections.

To execute the API examples via `curl`, set these environment variables:

* ``AUTH_TOKEN={your account's auth token}``
* ``V={api version you want to use}``

and replace the command parameters with those relevant to your account.



Access URL
==========
All URLs referenced in the documentation have the following base:

``https://api.qubole.com/api/${V}/``

where ${V} refers to the version of the API. Valid values of ${V} are currently ``v1``,
``v1.2`` and ``latest``.

Unless otherwise specified, the behavior of API requests remain same for ``v1`` and ``v1.2``.
We recommend ``v1.2`` or ``latest`` as this improves the retrieval of results.

cURL is a useful tool for testing out Qubole REST API calls from the command line.

The Qubole API is served over both HTTP and HTTPS. When using HTTPS, please ensure that the
certificates on the client machine are up-to-date. With an invalid local certificate,
you could use the ``--insecure`` option with cURL to have the call succeed.



Authentication
==============
API calls must be authenticated with a **Qubole API Token**.

Click on the `Profile` link on top of the page and navigate to the `User Profile` tab.
Then click on the `Generate Token` to generate a random API token.

Set the value of this API token
to the ``AUTH_TOKEN`` environment variable when running the API examples via `curl`.




API Types
=========
Broadly, the Qubole REST APIs can be divided into these categories:

:ref:`Command API <command-api>`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The Command APIs let you submit queries and commands, check the status of commands,
retrieve results and logs, or cancel commands. The Qubole Data Platform currently supports these
command types:

.. ToDo: link these types to their relevant individual docs/sections:

* Hive commands
* Hadoop jobs
* Pig jobs
* Presto commands
* Import and Export commands: that import/export data from data sources like MySQL, Postgres, Redshift
   to/from Hive tables/partitions or S3 folders.
* Shell commands



:ref:`Hive Metadata API <hive-metadata-api>`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
These APIs provide a set of read-only views that describe your Hive tables and their metadata.


:ref:`Cluster API <cluster-api>`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Cluster APIs let you launch and terminate clusters, and get information about a running
cluster. These APIs are meant for advanced users.



:ref:`DbTap API <dbtap-api>`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
A DbTap identifies an external end point for import/export of data from QDS, such as a MySQL instance.
The DbTap APIs let you create, view, edit or delete a DbTap.


:ref:`Scheduler API <scheduler-api>`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The Scheduler APIs let you schedule any command or workflow to run at regular intervals.


:ref:`Reports API <reports-api>`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
These APIs let you view aggregated statistical and operational data for your commands.
