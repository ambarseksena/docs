.. _cluster-api:

#########################
Cluster API
#########################

.. toctree::
    :maxdepth: 2
    :titlesonly:
    :numbered: 1

    list_clusters
    create_new_cluster

