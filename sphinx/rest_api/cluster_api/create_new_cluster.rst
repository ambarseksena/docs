Create a New Cluster
====================


..  http:post:: /clusters

    You can create a new cluster for a different workload that you would like to run in
    parallel with your pre-existing workloads.

    You might want to run workloads across different
    regions, or on different types of instances, or there could be other reasons for creating a
    new cluster.


+-----------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| Resource URI          | /clusters                                                                                                                                             |
+-----------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| Request Type          | POST                                                                                                                                                  |
+-----------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| Supporting Versions   | v1.2                                                                                                                                                  |
+-----------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| Return Value          | JSON object representing the created cluster. All the attributes mentioned here will be returned except when otherwise specified or when redundant.   |
+-----------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+



.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+-----------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Parameter                                     | Description                                                                                                                                                                                                                   |
+===============================================+===============================================================================================================================================================================================================================+
| **label**                                     | A list of labels that identify the cluster. At least one label must be provided when creating a cluster.                                                                                                                      |
+-----------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| |ec2settings|_                                | Amazon EC2 Settings                                                                                                                                                                                                           |
+-----------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| :ref:`hadoop-settings`                        | Hadoop Cluster Settings                                                                                                                                                                                                       |
+-----------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| :ref:`security-settings`                      | Instance Security Settings                                                                                                                                                                                                    |
+-----------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| :ref:`presto-settings`                        | Presto Settings                                                                                                                                                                                                               |
+-----------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| disallow_cluster_termination                  | Prevent auto-termination of the cluster after a prolonged period of disuse. Default: ``false``                                                                                                                                |
+-----------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| enable_ganglia_monitoring                     | Enable `Ganglia <http://ganglia.sourceforge.net/>`__ monitoring for the cluster. Default: ``false``                                                                                                                           |
+-----------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| node_bootstrap_file                           | A file that gets executed on every node of the cluster at boot time. You can use this to customize your cluster nodes by setting up environment variables, installing required packages, etc. Default: ``node_bootstrap.sh``  |
+-----------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

.. |ec2settings| replace:: **ec2_settings**
.. _ec2settings: ec2-settings_

.. _ec2-settings:

ec2_settings
------------


+--------------------------------------+---------------------------------------------------------------------------------------------------------+
| Parameter                            | Description                                                                                             |
+======================================+=========================================================================================================+
| **compute_access_key**               | EC2 Access Key *(Note: this field is not visible to non-admin users)*                                   |
+--------------------------------------+---------------------------------------------------------------------------------------------------------+
| **compute_secret_key**               | EC2 Secret Key *(Note: this field is not visible to non-admin users)*                                   |
+--------------------------------------+---------------------------------------------------------------------------------------------------------+
| aws_region                           | AWS region to create the cluster in.                                                                    |
|                                      | Default: ``us-east-1``. Valid values: ``us-east-1``, ``us-west-2``, ``eu-west-1``, ``ap-southeast-1``   |
+--------------------------------------+---------------------------------------------------------------------------------------------------------+
| aws_preferred_availability_zone      | The availability zone in the region to create the cluster in. Default: ``any``                          |
+--------------------------------------+---------------------------------------------------------------------------------------------------------+

.. _hadoop-settings:

hadoop_settings
----------------

+----------------------------+-----------------------------------------------------------------------------------------------+
| Parameter                  | Description                                                                                   |
+============================+===============================================================================================+
| master_instance_type       | The instance type to use for the Hadoop master node. Default: ``m1.large``                    |
+----------------------------+-----------------------------------------------------------------------------------------------+
| slave_instance_type        | The instance type to use for the Hadoop slave nodes. Default: ``m1.xlarge``                   |
+----------------------------+-----------------------------------------------------------------------------------------------+
| initial_nodes              | Number of nodes to start the cluster with. Default: ``1``                                     |
+----------------------------+-----------------------------------------------------------------------------------------------+
| max_nodes                  | Maximum number of nodes the cluster may be auto-scaled up to. Default: ``1``                  |
+----------------------------+-----------------------------------------------------------------------------------------------+
| custom_config              | Custom Hadoop configuration overrides. Default: *blank*                                       |
+----------------------------+-----------------------------------------------------------------------------------------------+
| slave_request_type         | Purchasing option for slave instances.                                                        |
|                            | Default: ondemand. Valid values: ``ondemand``, ``hybrid``, ``spot``                           |
+----------------------------+-----------------------------------------------------------------------------------------------+
| spot_instance_settings     | Purchase options for spot instances. Valid only when slave_request_type is hybrid or spot     |
+----------------------------+-----------------------------------------------------------------------------------------------+
| fairscheduler_settings     | Fair scheduler configuration options                                                          |
+----------------------------+-----------------------------------------------------------------------------------------------+


.. _spot-instance-settings:

spot_instance_settings
------------------------

+---------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------+
| Parameter                             | Description                                                                                                                                             |
+=======================================+=========================================================================================================================================================+
| maximum_bid_price_percentage          | Maximum value to bid for spot instances, expressed as a percentage of the base price for the slave node instance type. Default: ``100``                 |
+---------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------+
| timeout_for_request                   | Timeout for a spot instance request in minutes. Default ``5``                                                                                           |
+---------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------+
| maximum_spot_instance_percentage      | Maximum percentage of instances that may be purchased from the AWS Spot market. Valid only when slave_request_type is ‘hybrid’. Default value: ``50``   |
+---------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------+


.. _fairscheduler-settings:

fairscheduler_settings
-----------------------

+------------------------------+------------------------------------------------------------------------------------------+
| Parameter                    | Description                                                                              |
+==============================+==========================================================================================+
| fairscheduler_config_xml     | XML string with custom configuration parameters for the fair scheduler. Default: *blank* |
+------------------------------+------------------------------------------------------------------------------------------+
| default_pool                 | The default pool for the fair scheduler. Default: *blank*                                |
+------------------------------+------------------------------------------------------------------------------------------+


.. _security-settings:

security_settings
------------------

+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
| Parameter               | Description                                                                                                              |
+=========================+==========================================================================================================================+
| encrypted_ephemerals    | Encrypt the ephemeral drives on the instance. Default: ``false``                                                         |
+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
| customer_ssh_key        | SSH key to use to login to the instances. Default: *none*. (*Note: this field will not be visible to non-admin users*)   |
+-------------------------+--------------------------------------------------------------------------------------------------------------------------+

.. _presto-settings:

presto_settings
----------------

+-----------------------+----------------------------------------------------------+
| Parameter             | Description                                              |
+=======================+==========================================================+
| is_presto_enabled     | Enable `Presto <http://prestodb.io/>`_ on the cluster    |
+-----------------------+----------------------------------------------------------+
| custom_config         | Custom Presto configuration overrides. Default: blank    |
+-----------------------+----------------------------------------------------------+


Example
---------
**Goal**: Create a cluster called 'pig_workflow' in the 'us-west 2' AWS region


    .. sourcecode:: http

        curl -X POST -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type:application/json" \
        -d '{
              "cluster": {
                "ec2_settings": {
                  "aws_region": "us-west-2",
                  "compute_secret_key": "dummydummydummydummydummydummydummydummy",
                  "compute_access_key": "dummydummydummydummy"
                },
                "label": [
                  "pig_workflow",
                  "us_west"
                ]
              }
            }' \
        http://api.qubole.com/api/v1.2/clusters


    .. sourcecode:: http

        POST /clusters HTTP/1.1
        Host: api.qubole.com
        X-AUTH-TOKEN: dummydummydummy
        Content-Type: application/json

**Response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json; charset=utf-8

        {
          "cluster": {
            "state": "DOWN",
            "presto_settings": {
                "is_presto_enabled": false,
                "custom_config": null
            },
            "disallow_cluster_termination": false,
            "security_settings": {
                "encrypted_ephemerals": false
            },
            "enable_ganglia_monitoring": false,
            "hadoop_settings": {
                "master_instance_type": "m1.large",
                "fairscheduler_settings": {
                    "default_pool": null
                },
                "max_nodes": 2,
                "slave_instance_type": "m1.xlarge",
                "slave_request_type": "ondemand",
                "initial_nodes": 2,
                "custom_config": null
            },
            "node_bootstrap_file": null,
            "label": [
                "pig_workflow",
                "us_west"
            ],
            "id": 1711,
            "ec2_settings": {
                "compute_secret_key": "dummydummydummydummydummydummydummydummy",
                "compute_validated": true,
                "compute_access_key": "dummydummydummydummy",
                "aws_region": "us-west-2",
                "aws_preferred_availability_zone": "Any"
            }
          }
        }

