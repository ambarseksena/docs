List all Clusters
=================

..  http:get:: /clusters

    Get configuration details of all clusters in the account.

+-----------------------+--------------------------------------------------------+
| Resource URI          | /clusters                                              |
+-----------------------+--------------------------------------------------------+
| Request Type          | GET                                                    |
+-----------------------+--------------------------------------------------------+
| Supporting Versions   | v1.2                                                   |
+-----------------------+--------------------------------------------------------+
| Return Value          | A JSON array with objects representing the clusters.   |
+-----------------------+--------------------------------------------------------+




    **Example: List All Clusters**:

    .. sourcecode:: bash

      curl -X GET -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type:application/json" http://api.qubole.com/api/v1.2/clusters

    .. sourcecode:: http

      GET /clusters HTTP/1.1
      Host: api.qubole.com
      X-AUTH-TOKEN: dummydummydummy
      Accept: application/json

    **Example response**:

    .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json; charset=utf-8

      [{
          "cluster": {
              "state": "DOWN",
              "presto_settings": {
                  "is_presto_enabled": false,
                  "custom_config": null
              },
              "disallow_cluster_termination": false,
              "security_settings": {
                  "encrypted_ephemerals": false
              },
              "enable_ganglia_monitoring": false,
              "hadoop_settings": {
                  "master_instance_type": "m1.large",
                  "fairscheduler_settings": {
                      "default_pool": null
                  },
                  "max_nodes": 2,
                  "slave_instance_type": "m1.xlarge",
                  "slave_request_type": "ondemand",
                  "initial_nodes": 2,
                  "custom_config": null
              },
              "node_bootstrap_file": null,
              "label": [
                  "default"
              ],
              "id": 1711,
              "ec2_settings": {
                  "compute_secret_key": "dummydummydummydummydummydummydummydummy",
                  "compute_validated": true,
                  "compute_access_key": "dummydummydummydummy",
                  "aws_region": "us-east-1",
                  "aws_preferred_availability_zone": "Any"
              }
          }
      }]
