.. Qubole Documentation documentation master file, created by
   sphinx-quickstart on Mon May 19 11:44:45 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Qubole Data Platform Documentation
==================================

Contents:

.. toctree::
    :maxdepth: 2

    rest_api/index.rst
    presto/presto_qubole/index.rst
    aws/index.rst
    gce/index.rst

..    presto/presto_src_docs/index.rst





