.. Qubole AWS Documentation master file, created by
   sphinx-quickstart on Mon May 19 11:14:22 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#####################################
AWS Quick Start Guide
#####################################

This document is intended for new users to quickly start up on Qubole Data Services by running simple Hive queries.



Getting Started with Qubole on AWS
==================================

Contents:


How to Sign up
----------------

1. Go to http://www.qubole.com/.

2. Click **Sign Up for Free**. The following screen is displayed.

   .. image:: images/New_user_1.JPG

3. Provide the required information and click **Next**. The following screen is displayed. 

   .. image:: images/New_user_2.JPG

4. Enter your Email ID, Full Name, and answer the simple question. Click **CREATE MY FREE ACCOUNT**. You will receive an email, to the email ID that you provided, with an activation code. You can choose to confirm your account by clicking on the link sent to you in the email or copy and paste the activation code in the signup window. Sign up and you will be provided with a free trial account.

5. After sign up, you can use the following login page to login into Qubole Data Services.

   .. image:: images/Login_screen.JPG

6. Once you login, you will see the **Analyze Pane**.

   .. image:: images/landing_page.jpg

7. The **Analyze Pane** consists of several tabs that help you to perform various tasks. 

* The **History** tab lets you retrieve historical command information.

* The **Composer** tab lets you perform various tasks like running Hive queries, Hadoop streaming jobs, creating required commands, importing and exporting data, and so on. 
    Scroll towards the right side and you will see few tabs such as, **History**, **S3Exporer**, **TableExplorer**, and **ExprEvaluator**. A click on the **TableExplorer** tab will list all the tables that are available. All users have access to two preconfigured tables namely, **default_qubole_airline_origin_dest** and **default_qubole_memetracker**. Click the [+] next to the table name and detail information about the table is displayed.

   .. image:: images/Table_explorer.jpg
  
* The **Data Wizard** tab lets you connect to an external repository, view data, and import necessary data.

* The **Hive Bootstrap** tab lets you configure the Hive settings for all your Hive queries. 

* The **Sessions** tab lets you create new sessions, run hive commands, and control auto scaling in the Hadoop cluster. 

How to run your first Hive query, extract sample rows, and analyze data
-------------------------------------------------------------------------
1. In the **Analyze Pane**, click the **Composer** tab. You will see a drop-down list, just below the tabs, with a list of tasks that you can perform. By default, **Hive Query** is selected or displayed. 

2. To run a Hive query, ensure that the **Hive Query** is selected from the drop-down list. In the **Composer** window, type a simple query. For example::
	``show tables;``

3. Scroll down the screen and click **Submit**. 

4. The Sample Results are shown in the **Sample Results** area. 

	**Note:** If the query is successful, the Log area shows the status of the query as OK and displays the time taken to run the query. Also, next to the Query, the status is shown as **Succeeded**. You can also click on the History tab to see the query status. A green thumbs-up sign next to the query indicates the query is successful.

5. To execute another query, click **Clear**. This clears the command window. 

6. Now type and execute any other query in the **Composer** window. For example::
	select * from default_qubole_memetracker limit 10;

7. Scroll down the screen and click **Submit** to see the results.

	**Note:** The query takes a little time if a large amount of data has to be fetched.

8. To analyze the data, for example to find the total number of rows in a table corresponding to August 2008, submit the following query:::
	``select count(*) from default_qubole_memetracker where month="2008-08";``

	**Note:** This query is more complex than the previous queries and requires additional resources. In the background, Qubole Data Service provisions a Hadoop cluster. 

	This can take a couple of minutes. When the query is being processed, the status of the query is shown as **In Progress**. Once it is processed successfully, the status changes to **Succeeded**.

**Congratulations!!!** You have just executed your first Hive query on the Qubole Data Service!
Drop an email to support@qubole.com  and one of our engineers will get back to you and help you onboard. 

Glossary terms used
====================

.. * :term:`AWS:` Amazon Web Services
.. * :ref:`2`
.. * :ref:`search`

